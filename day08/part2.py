import sys
import re
import copy

with open('input.txt') as file:
    array = file.readlines()

instructions = []
index = 0
for line in array:
    match = re.match("^(\w+)\s([-+]\d+)$", line)
    instructions.append({"op": match.group(1), "count": int(match.group(2).strip()), "visited": False, "index": index})
    index += 1


def brute_force():
    for i in range(len(instructions)):
        instr_copy = copy.deepcopy(instructions)

        if instr_copy[i]["op"] == "nop":
            instr_copy[i]["op"] = "jmp"
        elif instr_copy[i]["op"] == "jmp":
            instr_copy[i]["op"] = "nop"
        else:
            continue

        run_brute(instr_copy)


def run_brute(instruction_list: list):
    accumulator = 0
    index = 0
    while True:
        instruction = instruction_list[index]

        if instruction["index"] == len(instruction_list) - 1:
            if instruction["op"] == "acc":
                accumulator += instruction["count"]
            print(accumulator)
            break

        if instruction["visited"]:
            break

        if instruction["op"] == "acc":
            accumulator += instruction["count"]
            index += 1
        elif instruction["op"] == "jmp":
            index += instruction["count"]
        else:
            index += 1

        instruction["visited"] = True


brute_force()
