import sys
import re

with open('input.txt') as file:
    array = file.readlines()

instructions = []
for line in array:
    match = re.match("^(\w+)\s([-+]\d+)$", line)
    instructions.append({"op": match.group(1), "count": int(match.group(2).strip()), "visited": False})

accumulator = 0
index = 0
last_index = 0

while True:
    instruction = instructions[index]
    if instruction["visited"] == True:
        print(accumulator)
        break
    else:
        last_index = index

    if instruction["op"] == "acc":
        accumulator += instruction["count"]
        index += 1
    elif instruction["op"] == "jmp":
        index += instruction["count"]
    else:
        index += 1

    instruction["visited"] = True
