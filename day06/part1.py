import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

answer_total = 0

group_answers = []
for index,line in enumerate(array):
    if line != '\n':
        group_answers.extend(list(line.strip()))
    else:
        unique = set(group_answers)
        answer_total += len(unique)
        group_answers = []

    if index == len(array) - 1:
        unique = set(group_answers)
        answer_total += len(unique)
        group_answers = []

print(answer_total)
