import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

answer_total = 0

group_answers = []
group_count = 0
for index,line in enumerate(array):
    if line != '\n':
        group_answers.extend(list(line.strip()))
        group_count += 1
    else:
        unique=set(group_answers)
        for char in unique:
            if group_answers.count(char) == group_count:
                answer_total += 1
        group_answers = []
        group_count = 0

    if index == len(array) - 1:
        unique=set(group_answers)
        for char in unique:
            if group_answers.count(char) == group_count:
                answer_total += 1
        group_answers = []
        group_count = 0


print(answer_total)
