import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
ecl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
valid_count = 0
passports = []
values = []

for index,line in enumerate(array):
    if(line != '\n'):
        values += line.split()
    elif(line == '\n'):
        passports.append(values)
        values = []

    if(index == (len(array) - 1)):
        passports.append(values)


for passport in passports:
    valid = True
    
    # Check for missing
    for field in fields:
        found = False
        for item in passport:
            if item[:3] == field:
                found = True
        if not found:
            valid = False

    for field in fields:
        for item in passport:
            if item[:3] != field:
                continue

            value = item[4:]
            if ((field == 'byr' and 1920 <= int(value) <= 2002)
                or (field == 'iyr' and 2010 <= int(value) <= 2020)
                or (field == 'eyr' and 2020 <= int(value) <= 2030)
                or (
                    (field == 'hgt' and (m1 := re.match('(\d+)(\w+)',value)).group(2) == 'cm' and 150 <= int(m1.group(1)) <= 193) or
                    (field == 'hgt' and (m2 := re.match('(\d+)(\w+)', value)).group(2) == 'in' and 59 <= int(m2.group(1)) <= 76))
                or (field == 'hcl' and re.match('^#[0-9a-f]{6}$', value) != None)
                or (field == 'ecl' and value in ecl)
                or (field == 'pid' and re.match('^[0-9]{9}$',value) != None)):
                continue
            else:
                valid = False

    if valid:
        valid_count += 1

print(valid_count)
