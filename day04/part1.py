import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

valid_count = 0
passports = []
values = []

for index,line in enumerate(array):
    if(line != '\n'):
        values += line.split()
    elif(line == '\n'):
        passports.append(values)
        values = []

    if(index == (len(array) - 1)):
        passports.append(values)


for passport in passports:
    valid = True
    for field in fields:
        found = False
        for item in passport:
            if item[:3] == field:
                found = True
        if not found:
            valid = False

    if valid:
        valid_count += 1

print(valid_count)
