import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

line_length = len(array[0])-1

index = 3 
trees_count= 0
for i in range(len(array) - 1):
    if(array[i+1][index%line_length] =='#'):
        trees_count += 1
    index += 3

print(trees_count)
