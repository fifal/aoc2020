import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

line_length = len(array[0])-1

index_3 = 3 
trees_count= 0
for i in range(len(array) - 1):
    if(array[i+1][index_3%line_length] =='#'):
        trees_count += 1
    index_3 += 3

trees_count_1 = 0
index_1 = 1
for i in range(len(array) - 1):
    if(array[i+1][index_1%line_length] == '#'):
        trees_count_1 +=1
    index_1 += 1

trees_count_5 = 0
index_5 = 5
for i in range(len(array) - 1):
    if(array[i+1][index_5%line_length] == '#'):
        trees_count_5 +=1
    index_5 += 5

trees_count_7 = 0
index_7 = 7
for i in range(len(array) - 1):
    if(array[i+1][index_7%line_length] == '#'):
        trees_count_7 +=1
    index_7 += 7

trees_count_1_1 = 0
index_1_1 = 1
for i in range(0,len(array)-2,2):
    if(array[i+2][index_1_1%line_length] == '#'):
       trees_count_1_1 +=1
    index_1_1 += 1

print(trees_count * trees_count_1 * trees_count_5 * trees_count_7 * trees_count_1_1)
