import sys
import re
from math import floor

array = []

with open('input.txt') as file:
    array = file.readlines()

max_row = 127
max_seats = 7
seat_ids = []

for line in array:
    p1 = line[:7]
    p2 = line[7:].strip()

    current_low = 0
    current_up = max_row
    last_action = ''
    row = 0
    seat = 0

    for ch in p1:
        diff = (current_up - current_low)
        if ch == 'F':
            current_up = current_up - round(diff/ 2)
        else:
            current_low = current_low + round(diff/ 2)

        last_action = ch

    if last_action == 'F':
        row = current_low
    else:
        row = current_up

    current_low = 0
    current_up = max_seats

    for ch in p2:
        diff = (current_up - current_low)
        if ch == 'L':
            current_up = current_up - round(diff / 2)
        else:
            current_low = current_low + round(diff / 2)
        
        last_action = ch

    if last_action == 'L':
        seat = current_low
    else:
        seat = current_up

    seat_ids.append(row * 8 + seat)

seat_ids.sort()

for i in range(len(seat_ids) - 1):
    if i == 0: continue
    if seat_ids[i] - seat_ids[i-1] == 1 and seat_ids[i+1] - seat_ids[i] == 1:
        continue
    else:
        if seat_ids[i] - seat_ids[i-1] > 1:
            print(seat_ids[i-1] + 1)
        else:
            print(seat_ids[i+1] - 1)
        break
