import sys
import re


class Bag:
    def __init__(self, bag_name: str, childs: list):
        self.bag_name = bag_name
        self.childs = childs

    def __str__(self):
        return f"{self.bag_name}: {self.childs}"


with open('input.txt') as file:
    array = file.readlines()

bags = dict()
for bag in array:
    bag_name = re.match("^(\w+\s\w+)", bag).group(1)
    matches = re.findall("(\d+)\s(\w+\s\w+)", bag)
    childs = []
    for match in matches:
        childs.append(match[1].strip())
    bag_obj = Bag(bag_name, childs)
    bags[bag_name] = bag_obj

my_bag = "shiny gold"
count = 0
outer_most = []


def find(name: str, outer: str):
    bag_name = bags[name].bag_name
    if bag_name == my_bag and outer != my_bag:
        outer_most.append(outer)
    else:
        for bag in bags[name].childs:
            find(bag, outer)


for bag_name in bags:
    find(bag_name, bag_name)

unique = set(outer_most)
print(len(unique))
