import sys
import re


class Bag:
    def __init__(self, bag_name: str, childs: dict):
        self.bag_name = bag_name
        self.childs = childs

    def __str__(self):
        return f"{self.bag_name}: {self.childs}"


with open('test.txt') as file:
    array = file.readlines()

bags = dict()
for bag in array:
    bag_name = re.match("^(\w+\s\w+)", bag).group(1)
    matches = re.findall("(\d+)\s(\w+\s\w+)", bag)
    childs = dict()
    count = 0
    for match in matches:
        childs[match[1]] = match[0]
    bag_obj = Bag(bag_name, childs)
    bags[bag_name] = bag_obj

my_bag = bags["shiny gold"]


def countF(bag: Bag, count: int):
    if len(bag.childs) > 0:
        for child in bag.childs:
            count += int(bag.childs[child]) * countF(bags[child], count)
        count += len(bag.childs)
        return count
    else:
        count += 1
        return count


count = countF(my_bag, 0)
print(count - 1)

# v   b        g
# 0 + 2*1 + 2 + 2*1 + 2 + 0 + 2*1 + 2 + 2*1
