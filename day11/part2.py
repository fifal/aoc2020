import sys
import re
import copy

with open('test.txt') as file:
    array = file.readlines()


def count_occupied(x: int, y: int, seats_array: array):
    occupied_count = 0
    occupied = '#'
    empty = 'L'

    for yy in range(y, 0):
        char = seats_array[yy][]





    return occupied_count


seats = []
for line in array:
    seats.append(list(line.strip()))

while (True):
    seats_copy = copy.deepcopy(seats)
    for index_y, seat_row in enumerate(seats):
        for index_x, seat in enumerate(seat_row):
            count = count_occupied(index_x, index_y, seats)
            if count == 0 and seat != ".":
                seats_copy[index_y][index_x] = "#"

            if count >= 5 and seat != ".":
                seats_copy[index_y][index_x] = "L"

    if seats_copy == seats:
        seats = seats_copy
        break

    seats = seats_copy

cnt = 0
for x in seats:
    for seat in x:
        if seat == "#":
            cnt += 1

print(cnt)
