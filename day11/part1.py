import sys
import re
import copy

with open('input.txt') as file:
    array = file.readlines()


def count_occupied(x: int, y: int, seats_array: array):
    occupied_count = 0
    occupied = '#'

    for i in range(-1, 2):
        for j in range(-1, 2):
            if i == 0 and j == 0:
                continue

            index_xx = x + i
            index_yy = y + j

            if 0 <= index_xx < len(seats_array[0]) and 0 <= index_yy < len(seats_array):
                char = seats_array[index_yy][index_xx]
                if char == occupied:
                    occupied_count = occupied_count + 1

    return occupied_count


seats = []
for line in array:
    seats.append(list(line.strip()))

while (True):
    seats_copy = copy.deepcopy(seats)
    for index_y, seat_row in enumerate(seats):
        for index_x, seat in enumerate(seat_row):
            count = count_occupied(index_x, index_y, seats)
            if count == 0 and seat != ".":
                seats_copy[index_y][index_x] = "#"

            if count >= 4 and seat != ".":
                seats_copy[index_y][index_x] = "L"

    if seats_copy == seats:
        seats = seats_copy
        break

    seats = seats_copy

cnt = 0
for x in seats:
    for seat in x:
        if seat == "#":
            cnt += 1

print(cnt)
