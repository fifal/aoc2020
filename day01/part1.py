import sys

array = []

with open('input.txt') as file:
    array = file.readlines()

for number in array:
    for number2 in array:
        if (int(number.strip()) + int(number2.strip())) == 2020:
            print(int(number) * int(number2))
            sys.exit()
