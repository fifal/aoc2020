import sys

array = []

with open('input.txt') as file:
    array = file.readlines()

for number in array:
    for number2 in array:
        for number3 in array:
            if (int(number.strip()) + int(number2.strip()) + int(number3.strip())) == 2020:
                print(int(number) * int(number2) * int(number3))
                sys.exit()
