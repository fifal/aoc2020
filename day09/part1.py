import sys
import re

with open('input.txt') as file:
    array = file.readlines()

numbers = []
for index, line in enumerate(array):
    numbers.append(int(line.strip()))


def first_number(preamble_count: int):
    global numbers
    rest = numbers[preamble_count:]

    for index, number in enumerate(rest):
        combination = []
        preamble = numbers[index:index+preamble_count]
        for p1 in preamble:
            for p2 in preamble:
                combination.append(p1 + p2)

        if number not in combination:
            print(number)
            break

first_number(25)
