import sys
import re

with open('input.txt') as file:
    array = file.readlines()

numbers = []
for index, line in enumerate(array):
    numbers.append(int(line.strip()))


def first_number(preamble_count: int, max_set_size: int):
    global numbers
    rest = numbers[preamble_count:]

    invalid_number = 0
    for index, number in enumerate(rest):
        combination = []
        preamble = numbers[index:index+preamble_count]
        for p1 in preamble:
            for p2 in preamble:
                combination.append(p1 + p2)

        if number not in combination:
            invalid_number = number
            break

    for index, number in enumerate(rest):
        for i in range(max_set_size):
            slice = numbers[index:index+i]
            if sum(slice) == invalid_number:
                slice.sort()
                result = slice[0] + slice[-1]
                print(result)
                return


first_number(25, 100)
