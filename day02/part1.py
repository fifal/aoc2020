import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

total_count = 0
for line in array:
    m = re.match('^(\d+)-(\d+)\s(\w):\s(\w+)$', line)
    min_num = int(m.group(1))
    max_num = int(m.group(2))
    req_char = m.group(3)
    password = m.group(4)

    count = 0
    for char in password:
        if(char == req_char): count += 1

    if min_num <= count <= max_num:
        total_count += 1

print(total_count)
