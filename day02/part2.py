import sys
import re

array = []

with open('input.txt') as file:
    array = file.readlines()

total_count = 0
for line in array:
    m = re.match('^(\d+)-(\d+)\s(\w):\s(\w+)$', line)
    min_num = int(m.group(1))
    max_num = int(m.group(2))
    req_char = m.group(3)
    password = m.group(4)

    if ((password[min_num - 1] == req_char and password[max_num - 1] != req_char)
            or (password[max_num - 1] == req_char and password[min_num -1] != req_char)):
        total_count += 1

print(total_count)
