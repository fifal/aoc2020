import sys
import re

with open('input.txt') as file:
    array = file.readlines()

facing = 'east'
position = {'x': 0, 'y': 0}


def change_facing(direction: str, val: int):
    global facing
    dirs = ['east', 'north', 'west', 'south']
    times = val / 90
    cur_index = dirs.index(facing)

    if direction == "R":
        cur_index -= times
    elif direction == "L":
        cur_index += times
    else:
        return

    cur_index = cur_index % 4
    facing = dirs[int(cur_index)]


def move_ship(direction: str, val: int):
    global position, facing
    if direction == "F":
        direction = facing[0].upper()

    if direction == "N":
        position['y'] += val
    elif direction == "S":
        position['y'] -= val
    elif direction == "W":
        position['x'] += val
    elif direction == "E":
        position['x'] -= val


for line in array:
    match = re.match("([A-Z])(\d+)", line)
    action = match.group(1)
    value = int(match.group(2))

    if action == 'R' or action == 'L':
        change_facing(action, value)

    move_ship(action, value)

distance = abs(position['x']) + abs(position['y'])
print(distance)
